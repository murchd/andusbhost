package nz.co.wetstone;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class USBBridge {
	Socket socket;
	public static void main(String[] args) {
		new USBBridge().run();
	}

	private void run() {
		try {
			Process p = Runtime.getRuntime().exec("/home/davidm/Apps/android-sdk-linux/platform-tools/adb forward tcp:38300 tcp:38300");
			Scanner sc = new Scanner(p.getErrorStream());
			if (sc.hasNext()) {
				while (sc.hasNext())
					System.out.println(sc.next());
				System.out.println("Cannot start the Android debug bridge");
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		initializeConnection();
	}

	/**
	 * Initialise connection to the phone
	 * 
	 */
	public void initializeConnection() {
		// Create socket connection
		try {
			socket = new Socket("localhost", 38300);
			PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
			out.write("Hello World");
			// in = new BufferedReader(new
			// InputStreamReader(socket.getInputStream()));
			Scanner sc = new Scanner(socket.getInputStream());
			// add a shutdown hook to close the socket if system crashes or
			// exists unexpectedly
			Thread closeSocketOnShutdown = new Thread() {
				public void run() {
					try {
						socket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			};
			Runtime.getRuntime().addShutdownHook(closeSocketOnShutdown);
		} catch (UnknownHostException e) {
			System.out.println("Socket connection problem (Unknown host)"
					+ e.getStackTrace());
		} catch (IOException e) {
			System.out.println("Could not initialize I/O on socket "
					+ e.getStackTrace());
		}
	}
}
